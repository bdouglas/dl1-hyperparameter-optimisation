prun --containerImage \
docker://gitlab-registry.cern.ch/aml/tutorials/dl1-hyperparameter-optimisation:latest \
--exec 'PYTHONPATH=/btagging/DL1_framework/Training:$PYTHONPATH \
python /btagging/train.py --configs %IN \
--validationfile /ctrdata/hp_scan.btagtutorial.DL1r_test_file \
--trainingfile /ctrdata/hp_scan.btagtutorial.DL1r_train_file \
--variables /ctrdata/hp_scan.btagtutorial.DL1r_varfile \
--validation_config /ctrdata/hp_scan.btagtutorial.DL1r_validconf \
--outputfile out.json \
--epochs 1 --large_file' \
--inDS user.mguth:user.mguth.dl1.hp.optimisation.configs_test \
--secondaryDS IN2#4#user.mguth.dl1r.files_hpscan \
--outDS user.${RUCIO_ACCOUNT}.hp.test.$(date +%Y%m%d%H%M%S) \
--forceStaged \
--noBuild \
--forceStagedSecondary \
--reusableSecondary=IN2 \
--outputs out.json \
--nFilesPerJob 3 \
--disableAutoRetry \
--cmtConfig nvidia-gpu \
--tmpDir /tmp \
--site ANALY_MANC_GPU_TEST