from __future__ import print_function
import json
import numpy as np
import pandas as pd
import argparse
from itertools import compress, product
import string


def GetParser():
    """Argparse option for Config Generator script."""
    parser = argparse.ArgumentParser(description=""" Options for creating
                                     config files for HP optimisation.""")

    parser.add_argument('--dicts_per_file', type=int, default=100)
    parser.add_argument('--config', type=str, default="HP_config.json")
    parser.add_argument('--output_base', type=str, default="config_new",
                        help="output file base")

    args = parser.parse_args()
    return args


args = GetParser()


def HP_dicts(dict_number):
    """Defines the structure of HP dictionary."""
    hp_id = "HP_%i" % dict_number
    {"HP_ID": hp_id,
     "lr": 0.005,
     "units": [512, 512, 512, 48, 36, 24, 12, 6],
     "activations": "relu",
     "batch_size": 300

     }


def explode(df, lst_cols, fill_value=''):
    # make sure `lst_cols` is a list
    if lst_cols and not isinstance(lst_cols, list):
        lst_cols = [lst_cols]
    # all columns except `lst_cols`
    idx_cols = df.columns.difference(lst_cols)

    # calculate lengths of lists
    lens = df[lst_cols[0]].str.len()

    if (lens > 0).all():
        # ALL lists in cells aren't empty
        return pd.DataFrame({
            col: np.repeat(df[col].values, lens)
            for col in idx_cols
        }).assign(**{col: np.concatenate(df[col].values) for col in lst_cols})\
          .loc[:, df.columns]
    else:
        # at least one list in cells is empty
        return pd.DataFrame({
            col: np.repeat(df[col].values, lens)
            for col in idx_cols
        }).assign(**{col: np.concatenate(df[col].values) for col in lst_cols})\
          .append(df.loc[lens == 0, idx_cols]).fillna(fill_value) \
          .loc[:, df.columns]


def Get_List(option, config_dict):
    if option == "lr" or option == "batch_size":
        if type(config_dict[option]) is list:
            if len(config_dict[option]) == 3:
                l_all = np.linspace(*config_dict[option])
            elif type(config_dict[option][0]) is list:
                l_all = config_dict[option][0]
        elif type(config_dict[option]) is float:
            l_all = [config_dict[option]]
        else:
            print("The instructions for", option, "are not correctly",
                  "implemented, see README.md for mor informations.")
            exit()
        return l_all

    if option == "activations":
        if type(config_dict[option]) is str:
            l_all = [config_dict[option]]
            return l_all
        if type(config_dict[option]) is not list:
            print("The instructions for", option, "are not correctly",
                  "implemented, see README.md for mor informations.")
            exit()
        is_list = [isinstance(x, list) for x in config_dict[option]]
        if not any(is_list):
            return [[elem for x in range(len(config_dict["units"]))]
                    for elem in config_dict[option]]
        config_dict[option] = config_dict[option][0]
        if len(config_dict[option]) != len(config_dict["units"]):
            print("The length of 'units' and 'activations'",
                  "don't match! ... aborting")
            exit()
        if type(config_dict[option]) is list:
            ac = pd.DataFrame([config_dict[option]])
            ac.columns = list(string.ascii_lowercase)[:len(ac.columns)]
            ab = [isinstance(x, list) for x in config_dict[option]]
            ab = list(compress(ac.columns, ab))
            for elem in ab:
                ac = explode(ac, elem)
            return ac.values.tolist()
    if option == "units":
        if type(config_dict[option]) is list:
            ac = pd.DataFrame([config_dict[option]])
            ac.columns = list(string.ascii_lowercase)[:len(ac.columns)]
            ab = [isinstance(x, list) for x in config_dict[option]]
            ab = list(compress(ac.columns, ab))
            for elem in ab:
                ac = explode(ac, elem)
            return ac.values.tolist()
        else:
            print("The instructions for", option, "are not correctly",
                  "implemented, see README.md for mor informations.")
            exit()


def my_product(inp):
    return (dict(zip(inp.keys(), values)) for values in product(*inp.values()))


def chunks(l, n):
    # For item i in a range that is a length of l,
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        yield l[i:i + n]


def Create_configs():
    """Creates config files for HP optimisation."""
    with open(args.config, 'r') as infile:
        config_dict = json.load(infile)

    variation_dict = {}
    for elem in config_dict:
        variation_dict[elem] = list(Get_List(elem, config_dict))

    variation_list = list(my_product(variation_dict))
    for i, elem in enumerate(variation_list):
        elem['hash'] = "HP_%i" % i
    print("Found", len(variation_list), "combinations.")
    variation_list = list(chunks(variation_list, args.dicts_per_file))
    path = 'configs'
    for i, elem in enumerate(variation_list):
        with open("%s/%s_%i.json" % (path, args.output_base, i), 'w') as outfile:
            json.dump(elem, outfile, indent=4)


if __name__ == '__main__':
    Create_configs()
