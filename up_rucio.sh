#!/bin/bash
TRAIN=/eos/user/m/mguth/public/btagging-ml_tutorial_files/MC16d_hybrid-training_sample-NN.h5
TEST=/eos/user/m/mguth/public/btagging-ml_tutorial_files/MC16d_hybrid_odd_100_PFlow-validation.h5

VALIDCONF=DL1_framework/Preprocessing/dicts/params_MC16D-ext_2018-PFlow_70-8M_mu.json
VARS=DL1_framework/Training/configs/DL1r_Variables.json

DATASET=user.${RUCIO_ACCOUNT}.dl1r.files_hpscan
DISK=UKI-NORTHGRID-MAN-HEP_SCRATCHDISK
rucio add-dataset ${DATASET}

TESTFILE=hp_scan.btagtutorial.DL1r_test_file
rucio upload ${TEST} --name ${TESTFILE} --rse ${DISK}
rucio attach ${DATASET} user.${RUCIO_ACCOUNT}:${TESTFILE} 

TRAINFILE=hp_scan.btagtutorial.DL1r_train_file
rucio upload ${TRAIN} --name ${TRAINFILE} --rse ${DISK}
rucio attach ${DATASET} user.${RUCIO_ACCOUNT}:${TRAINFILE}

CONFFILE=hp_scan.btagtutorial.DL1r_validconf
rucio upload ${VALIDCONF} --name ${CONFFILE} --rse ${DISK}
rucio attach ${DATASET} user.${RUCIO_ACCOUNT}:${CONFFILE}

VARFILE=hp_scan.btagtutorial.DL1r_varfile
rucio upload ${VARS} --name ${VARFILE} --rse ${DISK}
rucio attach ${DATASET} user.${RUCIO_ACCOUNT}:${VARFILE}

